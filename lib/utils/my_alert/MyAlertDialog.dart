import 'dart:io';

import '../../custom/constants.dart';
import 'package:flutter/material.dart';

/**
 * Created by daewubintara on
 * 27, April 2020 12.40
 */
class MyAlertDialog extends StatelessWidget {
  String title;
  String desc;
  Widget content;
  bool dismissable;
  List<Widget> buttons;

  static void show(BuildContext context,
      {Key key,
      String title,
      String desc,
      Widget content,
      bool dismissable,
      List<Widget> buttons}) {
    showDialog<void>(
        context: context,
        useRootNavigator: false,
        barrierDismissible: dismissable == null ? true : dismissable,
        builder: (_) {
          return MyAlertDialog(
              key: key,
              title: title,
              content: content,
              desc: desc,
              dismissable: dismissable,
              buttons: buttons);
        }).then((_) => FocusScope.of(context).requestFocus(FocusNode()));
  }

  static void hide(BuildContext context) => Navigator.pop(context);

  static void exitApp(BuildContext context) {
//    Navigator.of(context).pop(true);
    exit(0);
  }


  MyAlertDialog(
      {Key key,
      @required String title,
      String desc,
      bool dismissable,
      Widget content,
      List<Widget> buttons})
      : super(key: key) {
    this.desc = desc;
    this.content = content;
    this.title = title;
    this.buttons = buttons;
    this.dismissable = dismissable;
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        child: Center(
          child: Card(
            child: Wrap(
              children: [
                Container(
                  width: utilitiesWidget.sizeWidth(context) / 1.2,
                  child: _design(context),
                )
              ],
            ),
          ),
        ),
        onWillPop: () async => dismissable == null ? true : dismissable);
  }

  _design(BuildContext context) {
    return Column(
      children: [
        Container(
          decoration: BoxDecoration(
              color: PrimaryColor,
              borderRadius: BorderRadius.vertical(top: Radius.circular(4))),
          width: double.infinity,
          padding: EdgeInsets.all(paddingM),
          child: Image.asset("assets/images/img_logo.png", height: 30),
        ),
        _designBody(context),
        SizedBox(height: 10),
        buttons != null
            ? Container()
            : Container(
                padding: EdgeInsets.symmetric(horizontal: paddingM),
                width: double.infinity,
                child: FlatButton(
                  color: Colors.grey,
                  child: Text("Cancel",
                      style: Theme.of(context)
                          .textTheme
                          .caption
                          .copyWith(color: Colors.white)),
                  onPressed: () {
                    hide(context);
                  },
                ),
              ),
        buttons == null
            ? Container()
            : Padding(
                padding: const EdgeInsets.symmetric(horizontal: paddingXS),
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: _getButtons(context),
                ),
              ),
      ],
    );
  }

  _designBody(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      padding: EdgeInsets.all(paddingM),
      child: Column(
        children: [
          Wrap(
            children: [
              Text(
                title,
                style: Theme.of(context)
                    .textTheme
                    .title
                    .copyWith(color: Colors.black),
                textAlign: TextAlign.center,
              )
            ],
          ),
          desc == null
              ? Container()
              : Wrap(
                  children: [
                    Text(desc,
                        style: Theme.of(context).textTheme.subtitle,
                        textAlign: TextAlign.center)
                  ],
                ),
          content == null ? Container() : content,
        ],
      ),
    );
  }

  List<Widget> _getButtons(BuildContext context) {
    List<Widget> generate_buttons = [];
    if (buttons != null) {
      buttons.forEach(
        (button) {
          button = Container(
            width: (utilitiesWidget.sizeWidth(context) / 1.23) / buttons.length,
            padding: EdgeInsets.symmetric(horizontal: paddingXS),
            child: button,
          );
          generate_buttons.add(button);
        },
      );
    }
    return generate_buttons;
  }
}
