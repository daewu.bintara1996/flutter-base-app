import 'package:baseflutterapp/custom/constants.dart';
import 'package:flutter/material.dart';

import 'ShimmerLoading.dart';

/**
 * Created by daewubintara on
 * 28, July 2020 16.55
 */
class LoadingDetail extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ShimmerLoading(
        child: ListView(
            padding: EdgeInsets.symmetric(horizontal: paddingL),
            children: [
          SizedBox(height: 15),
          ClipRRect(
            borderRadius: BorderRadius.all(Radius.circular(10)),
            child: Container(
              color: Colors.grey[200],
              height: 250,
              width: double.maxFinite,
            ),
          ),
          SizedBox(height: 15),
          ClipRRect(
            borderRadius: BorderRadius.all(Radius.circular(10)),
            child: Container(
              color: Colors.grey[200],
              height: 40,
              width: double.maxFinite,
            ),
          ),
          SizedBox(height: 15),
          ClipRRect(
            borderRadius: BorderRadius.all(Radius.circular(10)),
            child: Container(
              color: Colors.grey[200],
              height: 100,
              width: double.maxFinite,
            ),
          ),
          SizedBox(height: 15),
          ClipRRect(
            borderRadius: BorderRadius.all(Radius.circular(10)),
            child: Container(
              color: Colors.grey[200],
              height: 100,
              width: double.maxFinite,
            ),
          ),
          SizedBox(height: 15),
          ClipRRect(
            borderRadius: BorderRadius.all(Radius.circular(10)),
            child: Container(
              color: Colors.grey[200],
              height: 100,
              width: double.maxFinite,
            ),
          ),
          SizedBox(height: 15),
        ]));
  }
}
