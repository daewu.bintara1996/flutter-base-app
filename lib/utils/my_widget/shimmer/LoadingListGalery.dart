import 'package:flutter/material.dart';

import 'ShimmerLoading.dart';

/**
 * Created by daewubintara on
 * 28, July 2020 16.55
 */
class LoadingListGalery extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ShimmerLoading(
        child: Wrap(
            children: [
              Container(
                margin: EdgeInsets.all(1),
                width: (MediaQuery.of(context).size.width/3)-2,
                height: (MediaQuery.of(context).size.width/3)-2,
                color: Colors.white,
              ),
              Container(
                margin: EdgeInsets.all(1),
                width: (MediaQuery.of(context).size.width/3)-2,
                height: (MediaQuery.of(context).size.width/3)-2,
                color: Colors.white,
              ),
              Container(
                margin: EdgeInsets.all(1),
                width: (MediaQuery.of(context).size.width/3)-2,
                height: (MediaQuery.of(context).size.width/3)-2,
                color: Colors.white,
              ),
              Container(
                margin: EdgeInsets.all(1),
                width: (MediaQuery.of(context).size.width/3)-2,
                height: (MediaQuery.of(context).size.width/3)-2,
                color: Colors.white,
              ),
              Container(
                margin: EdgeInsets.all(1),
                width: (MediaQuery.of(context).size.width/3)-2,
                height: (MediaQuery.of(context).size.width/3)-2,
                color: Colors.white,
              ),
              Container(
                margin: EdgeInsets.all(1),
                width: (MediaQuery.of(context).size.width/3)-2,
                height: (MediaQuery.of(context).size.width/3)-2,
                color: Colors.white,
              ),
              Container(
                margin: EdgeInsets.all(1),
                width: (MediaQuery.of(context).size.width/3)-2,
                height: (MediaQuery.of(context).size.width/3)-2,
                color: Colors.white,
              ),
              Container(
                margin: EdgeInsets.all(1),
                width: (MediaQuery.of(context).size.width/3)-2,
                height: (MediaQuery.of(context).size.width/3)-2,
                color: Colors.white,
              ),
              Container(
                margin: EdgeInsets.all(1),
                width: (MediaQuery.of(context).size.width/3)-2,
                height: (MediaQuery.of(context).size.width/3)-2,
                color: Colors.white,
              ),
            ],
          ),
        );
  }
}
