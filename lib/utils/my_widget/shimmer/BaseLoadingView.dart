import 'package:flutter/material.dart';

import 'LoadingDetail.dart';
import 'LoadingListGalery.dart';
import 'LoadingListGrid.dart';
import 'LoadingListVertical.dart';

/**
 * Created by daewubintara on
 * 28, July 2020 17.00
 */
class BaseLoadingView extends StatelessWidget {
  static String TYPE_VERTICAL = "TYPE_VERTICAL";
  static String TYPE_HORIZONTAL = "TYPE_HORIZONTAL";
  static String TYPE_GRID = "TYPE_GRID";
  static String TYPE_DETAIL = "TYPE_DETAIL";
  static String TYPE_GALERY = "TYPE_GALERY";
  static String TYPE_CIRCLE = "TYPE_CIRCLE";
  String type;

  BaseLoadingView({this.type});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    switch (type) {
      case "TYPE_VERTICAL":
        return LoadingListVertical();
        break;
      case "TYPE_HORIZONTAL":
        return LoadingListVertical();
        break;
      case "TYPE_GRID":
        return LoadingListGrid();
        break;
      case "TYPE_DETAIL":
        return LoadingDetail();
        break;
      case "TYPE_GALERY":
        return LoadingListGalery();
        break;
      case "TYPE_CIRCLE":
        return Center(
          child: Container(
            height: 35,
            width: 35,
            padding: EdgeInsets.all(8),
            child: CircularProgressIndicator(
              strokeWidth: 2,
            ),
          ),
        );
        break;
    }
  }
}
