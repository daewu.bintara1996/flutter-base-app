import 'package:baseflutterapp/custom/constants.dart';
import 'package:flutter/material.dart';

import 'ShimmerLoading.dart';

/**
 * Created by daewubintara on
 * 28, July 2020 16.55
 */
class LoadingListGrid extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ShimmerLoading(
        child: ListView(
            padding: EdgeInsets.symmetric(horizontal: paddingM),
            children: [
          SizedBox(height: 15),
          Row(
            children: [
              Expanded(
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: paddingS),
                  child: ClipRRect(
                    borderRadius: BorderRadius.all(Radius.circular(10)),
                    child: Container(
                      color: Colors.grey[200],
                      height: 200,
                      width: double.maxFinite,
                    ),
                  ),
                ),
              ),
              Expanded(
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: paddingS),
                  child: ClipRRect(
                    borderRadius: BorderRadius.all(Radius.circular(10)),
                    child: Container(
                      color: Colors.grey[200],
                      height: 200,
                      width: double.maxFinite,
                    ),
                  ),
                ),
              ),
            ],
          ),
          SizedBox(height: 15),
          Row(
            children: [
              Expanded(
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: paddingS),
                  child: ClipRRect(
                    borderRadius: BorderRadius.all(Radius.circular(10)),
                    child: Container(
                      color: Colors.grey[200],
                      height: 200,
                      width: double.maxFinite,
                    ),
                  ),
                ),
              ),
              Expanded(
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: paddingS),
                  child: ClipRRect(
                    borderRadius: BorderRadius.all(Radius.circular(10)),
                    child: Container(
                      color: Colors.grey[200],
                      height: 200,
                      width: double.maxFinite,
                    ),
                  ),
                ),
              ),
            ],
          ),
          SizedBox(height: 15),
          Row(
            children: [
              Expanded(
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: paddingS),
                  child: ClipRRect(
                    borderRadius: BorderRadius.all(Radius.circular(10)),
                    child: Container(
                      color: Colors.grey[200],
                      height: 200,
                      width: double.maxFinite,
                    ),
                  ),
                ),
              ),
              Expanded(
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: paddingS),
                  child: ClipRRect(
                    borderRadius: BorderRadius.all(Radius.circular(10)),
                    child: Container(
                      color: Colors.grey[200],
                      height: 200,
                      width: double.maxFinite,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ]));
  }
}
