import 'dart:async';

import '../../models/BaseRequest.dart';
import '../../router/router.dart';
import '../../utils/my_alert/MyAlertDialog.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_money_formatter/flutter_money_formatter.dart';
import 'package:intl/intl.dart';
import '../../custom/constants.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:html/parser.dart';

/**
 * Created by DAEWU on
 * 12, February, 2020
 */

var globalContext;

class UtilitiesWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    globalContext = context;
    // TODO: implement build
    return null;
  }

  void onDoneRefresh(Completer<void> refreshCompleter) async {
    refreshCompleter?.complete();
    refreshCompleter = Completer();
  }

  double sizeHeight(BuildContext context) {
    return MediaQuery.of(context).size.height;
  }

  double sizeWidth(BuildContext context) {
    return MediaQuery.of(context).size.width;
  }

  dismisKeyboard(BuildContext context) {
    return FocusScope.of(context).requestFocus(FocusNode());
  }

  showDataNotFound({String message, EdgeInsetsGeometry padding}) {
    return Container(
      padding: padding,
      child: Center(
        child: Text(
          message,
          style: TextStyle(color: Colors.grey),
          textAlign: TextAlign.center,
        ),
      ),
    );
  }

  myAppBar({BuildContext context, String title_bar, bool center_title}) {
    return PreferredSize(
        child: AppBar(
          elevation: title_bar=='MORE'?0:6,
          centerTitle: center_title,
          title: Text(title_bar,
              style: Theme.of(context).textTheme.title.copyWith(
                  color: Colors.white,
                  fontStyle: FontStyle.italic,
                  fontWeight: FontWeight.bold)),
        ),
        preferredSize: Size(sizeWidth(context), 55));
  }

  opacityAnimation({bool show, Widget child}) {
    return Stack(
      children: <Widget>[
        AnimatedOpacity(
          opacity: show ? 1.0 : 0.0,
          duration: Duration(milliseconds: 700),
          child: child,
        ),
        AnimatedOpacity(
          opacity: show ? 0.0 : 1.0,
          duration: Duration(milliseconds: 500),
          child: show
              ? Container(
                  height: 1,
                )
              : Center(
                  child: Container(
                    height: 35,
                    width: 35,
                    padding: EdgeInsets.all(8),
                    child: CircularProgressIndicator(
                      strokeWidth: 2,
                    ),
                  ),
                ),
        ),
      ],
    );
  }

  opacityAnimationPage({int position, List<Widget> childrens}) {
    if (childrens.length > 0) {
      return Stack(
        children: childrens.map((children) {
          new Container(
            child: AnimatedOpacity(
              opacity: 1.0,
              duration: Duration(milliseconds: 700),
              child: children,
            ),
          );
        }).toList(),
      );
    }
  }

  String rupiahFormater(String value) {
    if (value == null || value == 'null') {
      value = "0";
    }

    double amount = double.parse(value);
    FlutterMoneyFormatter fmf = FlutterMoneyFormatter(amount: amount);
    String c = fmf.output.nonSymbol.toString().replaceAll(".00", "");
    String fix = "Rp. " + c.replaceAll(",", ".");
    return fix;
  }

  String formattedDate({String format, String date}) {
    if (date == 'null') {
      return "";
    }

    DateFormat dateFormat = DateFormat(format);
    DateTime dateTime = dateFormat.parse(date);

    String formattedDate = DateFormat('dd MMMM yyyy').format(dateTime);
    return formattedDate;
  }

  String formattedDateGetDay({String format, String date}) {
    if (date == 'null') {
      return "";
    }

    DateFormat dateFormat = DateFormat(format);
    DateTime dateTime = dateFormat.parse(date);

    String formattedDate = DateFormat('EEEE').format(dateTime);
    switch(formattedDate){
      case "Monday":
        formattedDate = "Senin";
        break;
      case "Tuesday":
        formattedDate = "Selasa";
        break;
      case "Wednesday":
        formattedDate = "Rabu";
        break;
      case "Thursday":
        formattedDate = "Kamis";
        break;
      case "Friday":
        formattedDate = "Jum'at";
        break;
      case "Saturday":
        formattedDate = "Sabtu";
        break;
      case "Sunday":
        formattedDate = "Minggu";
        break;
    }

    return formattedDate;
  }

  String formattedDateGetMonth({String format, String date}) {
    if (date == 'null') {
      return "";
    }

    DateFormat dateFormat = DateFormat(format);
    DateTime dateTime = dateFormat.parse(date);

    String formattedDate = DateFormat('MMMM').format(dateTime);
    switch(formattedDate){
      case "January":
        formattedDate = "Januari";
        break;
      case "February":
        formattedDate = "Februari";
        break;
      case "March":
        formattedDate = "Maret";
        break;
      case "April":
        formattedDate = "April";
        break;
      case "May":
        formattedDate = "Mei";
        break;
      case "June":
        formattedDate = "Juni";
        break;
      case "July":
        formattedDate = "Juli";
        break;
      case "August":
        formattedDate = "Agustus";
        break;
      case "September":
        formattedDate = "September";
        break;
      case "October":
        formattedDate = "Oktober";
        break;
      case "November":
        formattedDate = "November";
        break;
      case "December":
        formattedDate = "Desember";
        break;
    }

    return formattedDate;
  }

  String formattedSimpleDate({String format, String date}) {
    if (date == 'null') {
      return "";
    }

    DateFormat dateFormat = DateFormat(format);
    DateTime dateTime = dateFormat.parse(date);

    String formattedDate = DateFormat('dd MMM yyyy').format(dateTime);
    return formattedDate;
  }

  String formattedDateTimeWithDay({String format, String date}) {
    if (date == 'null') {
      return "";
    }

    DateFormat dateFormat = DateFormat(format);
    DateTime dateTime = dateFormat.parse(date);

    String day = formattedDateGetDay(format: format, date: date);

    String formattedDate = DateFormat('dd MMMM yyyy HH:mm').format(dateTime);
    return '${day}, ${formattedDate}';
  }

  String formattedDateTime({String format, String date}) {
    if (date == 'null') {
      return "";
    }

    DateFormat dateFormat = DateFormat(format);
    DateTime dateTime = dateFormat.parse(date);

    String formattedDate = DateFormat('dd MMMM yyyy HH:mm').format(dateTime);
    return formattedDate;
  }

  String formattedSimpleDateTime({String format, String date}) {
    if (date == 'null') {
      return "";
    }

    DateFormat dateFormat = DateFormat(format);
    DateTime dateTime = dateFormat.parse(date);

    String formattedDate = DateFormat('dd MMM yyyy HH:mm').format(dateTime);
    return formattedDate;
  }

  void launchNewIntentURL(String link) async {
    final url = link;
    //    final url = 'https://www.google.com/maps/search/?api=1&query=$lat,$lon';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  Widget showErrorResspon({String text}) {
    return Center(
      child: Container(
        padding: EdgeInsets.all(10),
        decoration: BoxDecoration(
            color: PrimaryAssentColor,
            borderRadius: BorderRadius.all(Radius.circular(10))),
        child: Text(
          text,
          style: TextStyle(
            color: PrimaryColor,
          ),
          textAlign: TextAlign.center,
        ),
      ),
    );
  }

  String parseHtmlString(String htmlString) {
    var document = parse(htmlString);
    String parsedString = parse(document.body.text).documentElement.text;
    return parsedString;
  }

  Widget designInfoItem({String title, String info, bool isBold}) {
    if (isBold == null) {
      isBold = false;
    }

    return Container(
      padding: EdgeInsets.symmetric(horizontal: 0, vertical: 2),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Expanded(
            flex: 4,
            child: Text(title),
          ),
          Expanded(
            flex: 7,
            child: !isBold
                ? Text(": " + info)
                : Text(
                    ": " + info,
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
          ),
        ],
      ),
    );
  }

  Widget showInfoWidget({String text, IconData iconData}) {
    return Container(
      padding: EdgeInsets.all(10),
      margin: EdgeInsets.all(10),
      decoration: BoxDecoration(
          color: Colors.green[50],
          borderRadius: BorderRadius.all(Radius.circular(10))),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
//          Icon(iconData == null ? Icons.info_outline : iconData, color: PrimaryColor),
//          SizedBox(width: 8),
          Expanded(child: Text(
            text,
            style: TextStyle(color: PrimaryColor),
            textAlign: TextAlign.start,
          ))
        ],
      ),
    );
  }

  Gradient colorGradient() {
    return LinearGradient(
      colors: <Color>[Colors.green[700], Colors.green[600], Colors.green[500]],
    );
  }

  Gradient colorGradientVerticalBlack() {
    return LinearGradient(
      begin: Alignment.topCenter,
      end: Alignment.bottomCenter,
      colors: <Color>[Colors.black, Colors.transparent],
    );
  }
  void exceptionHandler({BuildContext context, BaseRequest data}){
    if(data.rc.toString() == '501'){
      Navigator.pushReplacementNamed(context, RouterName.error_page, arguments: data.error_text);
    } else if (data.rc.toString() == '502'){
      Navigator.pushNamed(context, RouterName.error_page, arguments: data.error_text);
    } else if (data.rc.toString() == '403'){
      Navigator.pushNamed(context, RouterName.login_register);
    } else if (data.rc.toString() == '411'){// UNTUK UPDATE APLIKASI
      MyAlertDialog.show(
          context,
          dismissable: false,
          title: "Aplikasi Update",
          desc: "${data.text}",
          buttons: [
            FlatButton(
              color: Colors.grey,
              child: Text("Tidak",
                  style: Theme.of(context)
                      .textTheme
                      .caption
                      .copyWith(color: Colors.white)),
              onPressed: () {
                MyAlertDialog.exitApp(context);
              },
            ),
            FlatButton(
              color: PrimaryColor,
              child: Text("Update",
                  style: Theme.of(context)
                      .textTheme
                      .caption
                      .copyWith(color: Colors.white)),
              onPressed: () {
                launchNewIntentURL(data.link);
              },
            ),
          ]
      );
    } else if (data.rc.toString() == '401'){// UNTUK ERROR NETWORK
      MyAlertDialog.show(
          context,
          dismissable: false,
          title: "Koneksi Error",
          desc: "${data.text}",
          buttons: [
            FlatButton(
              color: Colors.grey,
              child: Text("Keluar",
                  style: Theme.of(context)
                      .textTheme
                      .caption
                      .copyWith(color: Colors.white)),
              onPressed: () {
                MyAlertDialog.exitApp(context);
              },
            ),
            FlatButton(
              color: PrimaryColor,
              child: Text("Restart",
                  style: Theme.of(context)
                      .textTheme
                      .caption
                      .copyWith(color: Colors.white)),
              onPressed: () {
                MyAlertDialog.hide(context);
                Navigator.of(context).pushNamedAndRemoveUntil(RouterName.splash,  (Route<dynamic> route) => false, arguments: null);
              },
            ),
          ]
      );
    }
  }

}
