import 'dart:async';

import 'package:flutter/material.dart';
import '../../custom/constants.dart';

/**
 * Created by DAEWU on
 * 24, February, 2020
 */

class SliderImage extends StatefulWidget {
  ValueChanged<int> onTap;
  List<String> images;
  SliderImage({@required this.onTap, this.images});

  @override
  _SliderImageState createState() => _SliderImageState(onTap: onTap, images: images);
}

class _SliderImageState extends State<SliderImage> with TickerProviderStateMixin{
  TabController _tabController;
  Timer _timer;
  ValueChanged<int> onTap;
  List<String> images;
  _SliderImageState({@required this.onTap, this.images});

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    print("LENGTH === "+images.length.toString());
    _tabController = TabController(
      length: images.length,
      initialIndex: 0,
      vsync: this,
    );
    _timerSlider();
  }

  @override
  void dispose() {
    _timer.cancel();
    super.dispose();
  }

  _timerSlider(){
    const oneSec = const Duration(seconds: 3);
    _timer = new Timer.periodic(
      oneSec,
          (Timer timer) => setState(
            () {
              if (_tabController.index == images.length-1){
                _tabController.index = 0;
              } else {
                _tabController.index += 1;
              }
        },
      ),
    );
  }

  @override
  Widget build(BuildContext context) {

    List<Widget> wimages = List.generate(images.length, (int index){
      return InkWell(
        onTap: (){
          return onTap(index);
        },
        child: ClipRRect(
          borderRadius: BorderRadius.all(Radius.circular(10)),
          child: Container(
              child: Stack(
                children: <Widget>[
                  Positioned.fill(
                      child: Align(
                        alignment: Alignment.center,
                        child: CircularProgressIndicator(
                          valueColor: new AlwaysStoppedAnimation<Color>(Colors.white),
                        ),
                      )
                  ),
                  Image.network(images[index].toString(), fit: BoxFit.fill,
                    width: utilitiesWidget.sizeWidth(context)/0.8,)
                ],
              )
          ),
        ),
      );
    });

    List<Widget> indicators = List.generate(images.length, (int index){
      return InkWell(
        child: Container(
            alignment: Alignment.center,
            margin: EdgeInsets.all(2),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(10)),
              color: index == _tabController.index
                ? Colors.white : Colors.grey[300],
            ),
            height: 7,
            width: 7,
        ),
      );
    });

    return DefaultTabController(
        length: images.length,
        child: Column(
          children: [
            Expanded(
                child: TabBarView(
                  controller: _tabController,
                  children: wimages,
                )
            ),
            Container(
              alignment: Alignment.center,
              width: double.infinity,
              child: Center(
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisSize: MainAxisSize.min,
                  children: indicators,
                ),
              ),
            ),
            SizedBox(height: 10),
          ],
        )
    );
  }
}
