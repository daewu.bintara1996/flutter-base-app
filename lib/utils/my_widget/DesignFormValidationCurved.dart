import 'package:baseflutterapp/custom/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

/**
 * Created by DAEWU on
 * 28, February, 2020
 */

class DesignFormValidationCurved extends StatelessWidget {
  var initial, name;
  bool is_number;
  bool is_password;
  FormFieldValidator<String> validator;
  ValueChanged<String> onChanged;
  Color color_name;
  Function(String) onSubmited;
  FocusNode focusNode;
  TextInputAction textInputAction;
  double corner, padding;

  DesignFormValidationCurved(
      {this.name,
      this.initial,
      this.onChanged,
      this.is_number,
      this.is_password,
      this.validator,
      this.color_name,
      this.focusNode,
      this.textInputAction,
      this.corner,
      this.padding,
      this.onSubmited});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    if (is_number == null) {
      is_number = false;
    }

    if (is_password == null) {
      is_password = false;
    }
    if (color_name == null) {
      color_name = Colors.black;
    }
    if (corner == null){
      corner = 10;
    }

    if (padding == null){
      padding = 10;
    }

    return Container(
      padding: EdgeInsets.symmetric(horizontal: 14, vertical: 5),
      child: TextFormField(
        onFieldSubmitted: onSubmited,
        focusNode: focusNode,
        validator: validator,
        obscureText: is_password,
        inputFormatters:
            is_number ? [WhitelistingTextInputFormatter.digitsOnly] : null,
        initialValue: initial != null ? initial.toString() : "",
        onChanged: (val) {
          onChanged(val);
        },
        textInputAction: textInputAction==null ? TextInputAction.done : textInputAction,
        keyboardType:
            is_number ? TextInputType.number : TextInputType.emailAddress,
        decoration: InputDecoration(
          labelText: name,
          labelStyle: TextStyle(color: Colors.black),
          contentPadding: EdgeInsets.symmetric(horizontal: padding),
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(corner),
            borderSide: BorderSide(
              color: PrimaryColor,
            ),
          ),
        ),
      ),
    );
  }
}
