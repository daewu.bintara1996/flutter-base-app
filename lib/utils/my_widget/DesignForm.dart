import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

/**
 * Created by DAEWU on
 * 28, February, 2020
 */

class DesignForm extends StatelessWidget {
  var initial, name;
  bool is_number;
  bool is_password;
  ValueChanged<String> onChanged;
  DesignForm(this.name, this.initial, {this.onChanged, this.is_number, this.is_password});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    if(is_number==null){
      is_number = false;
    }

    if(is_password==null){
      is_password = false;
    }

    return Container(
      padding: EdgeInsets.symmetric(horizontal: 14, vertical: 5),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(initial),
          SizedBox(height: 2),
          Container(
            child: Material(
              elevation: 2,
              color: Colors.white,
              borderRadius: BorderRadius.all(Radius.circular(5)),
              child: TextFormField(
                obscureText: is_password,
                inputFormatters: is_number?[WhitelistingTextInputFormatter.digitsOnly]:null,
                initialValue: name!=null?name.toString():"",
                onChanged: (val) {
                  onChanged(val);
                },
                textInputAction: TextInputAction.done,
                keyboardType: is_number?TextInputType.number:TextInputType.emailAddress,
                decoration: InputDecoration(
                  contentPadding: EdgeInsets.symmetric(horizontal: 10),
                  border: InputBorder.none,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }


}
