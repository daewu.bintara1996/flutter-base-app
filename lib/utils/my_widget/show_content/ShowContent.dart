import 'package:baseflutterapp/utils/my_widget/shimmer/BaseLoadingView.dart';
import 'package:flutter/material.dart';

/**
 * Created by daewubintara on
 * 05, August 2020 11.34
 */
class ShowContent extends StatelessWidget {

  bool show; // REQUIRED
  Widget child; // REQUIRED
  String type; // if null returned BaseLoadingView().TYPE_VERTICAL
  ShowContent({this.show, this.child, this.type});

  @override
  Widget build(BuildContext context) {
    return opacityAnimation();
  }

  opacityAnimation({bool show, Widget child, String type}) {
    if(type == null){
      type = BaseLoadingView.TYPE_VERTICAL;
    }
    return Stack(
      children: <Widget>[
        AnimatedOpacity(
          opacity: show ? 1.0 : 0.0,
          duration: Duration(milliseconds: 700),
          child: child,
        ),
        AnimatedOpacity(
          opacity: show ? 0.0 : 1.0,
          duration: Duration(milliseconds: 500),
          child: show
              ? Container(
            height: 1,
          )
              : BaseLoadingView(type: type),
        ),
      ],
    );
  }

}