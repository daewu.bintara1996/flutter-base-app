import '../../custom/constants.dart';
import 'package:flutter/material.dart';

/**
 * Created by DAEWU on
 * 26, March, 2020
 */

class LoadingDialog extends StatelessWidget {

  static void show(BuildContext context,{Key key})=>showDialog<void>(
      context: context,
      useRootNavigator: false,
      barrierDismissible: false,
      builder: (_)=>LoadingDialog(key: key)).then((_) => FocusScope.of(context).requestFocus(FocusNode()));

  static void hide(BuildContext context)=>Navigator.pop(context);

  LoadingDialog({Key key}):super(key: key);

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        child: Center(
          child: Card(
            child: Container(
              height: 60,
              width: 60,
              padding: EdgeInsets.all(marginM),
              child: CircularProgressIndicator(),
            ),
          ),
        ),
        onWillPop: ()async=>false);
  }
}
