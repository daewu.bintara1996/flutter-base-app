import 'package:flutter/material.dart';

/**
 * Created by DAEWU on
 * 28, February, 2020
 */

class ItemListMore extends StatelessWidget {

  Function onPressed;
  String title;
  ItemListMore({@required this.title, this.onPressed});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        InkWell(
          onTap: onPressed,
          child: Container(
            padding: EdgeInsets.all(16),
            child: Row(
              children: [
                Expanded(
                    child: Text(
                      title,
                      style: Theme.of(context)
                          .textTheme
                          .subtitle
                          .copyWith(color: Colors.grey),
                    )
                ),
                Icon(Icons.arrow_forward_ios, color: Colors.grey, size: 15)
              ],
            ),
          ),
        ),
        Container(
          width: double.infinity,
          height: 0.6,
          color: Colors.grey,
        ),
      ],
    );
  }
}
