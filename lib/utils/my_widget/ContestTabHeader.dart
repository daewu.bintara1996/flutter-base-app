import 'package:flutter/material.dart';

/**
 * Created by DAEWU on
 * 24, February, 2020
 */

class ContestTabHeader extends SliverPersistentHeaderDelegate {
  final Widget widget;
  ContestTabHeader(
      this.widget,
      );
  @override
  Widget build(BuildContext context, double shrinkOffset, bool overlapsContent) {
    return widget;
  }

  @override
  double get maxExtent => 70.0;

  @override
  double get minExtent => 70.0;

  @override
  bool shouldRebuild(SliverPersistentHeaderDelegate oldDelegate) {
    return false;
  }
}