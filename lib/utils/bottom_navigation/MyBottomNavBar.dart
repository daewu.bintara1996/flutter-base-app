import 'package:flutter/material.dart';

/**
 * Created by DAEWU on
 * 15, February, 2020
 */

class MyBottomNavBar extends StatelessWidget {
  int currentIndex;
  Color backgroundColor;
  double elevation;
  double icon_size;
  bool showUnselectedLabels;
  Color selectedItemColor;
  Color unselectedItemColor;
  ValueChanged<int> onTap;
  List<MyBottomNavBarItem> items;
  BuildContext context;

  @override
  Widget build(BuildContext context) {
    return null;
  }

  MyBottomNavBarRoot({
    int currentIndex,
    double icon_size,
    Color backgroundColor,
    double elevation,
    BuildContext context,
    bool showUnselectedLabels,
    Color selectedItemColor,
    Color unselectedItemColor,
    ValueChanged<int> onTap,
    List<MyBottomNavBarItem> items,
  }) {
    this.context = context;
    this.icon_size = icon_size;
    this.currentIndex = currentIndex;
    this.backgroundColor = backgroundColor;
    this.elevation = elevation;
    this.showUnselectedLabels = showUnselectedLabels;
    this.selectedItemColor = selectedItemColor;
    this.unselectedItemColor = unselectedItemColor;
    this.onTap = onTap;

    List<Widget> items_widget = List.generate(items.length, (int index) {
      return _buildTabItem(
        item: items[index],
        index: index,
        onPressed: (index) => onTap(index),
      );
    });

    return BottomAppBar(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Container(
              height: 0.25, width: double.infinity, color: unselectedItemColor),
          Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: items_widget,
          )
        ],
      ),
      color: backgroundColor,
    );
  }

  _buildTabItem({
    MyBottomNavBarItem item,
    int index,
    ValueChanged<int> onPressed,
  }) {
    Color color =
        currentIndex == index ? selectedItemColor : unselectedItemColor;
    return Expanded(
      child: SizedBox(
        height: 56.0,
        child: Material(
          type: MaterialType.transparency,
          child: InkWell(
            onTap: () => onPressed(index),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                /// FOR ASSET IMAGE
//                Image.asset(item.iconData, color: color, height: widget.iconSize),
                Icon(item.icon, color: color, size: icon_size),
                SizedBox(height: 5),
                Text(
                  item.title,
                  style: Theme.of(context).textTheme.caption.copyWith(
                        color: color,
                      ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class MyBottomNavBarItem {
  MyBottomNavBarItem({this.icon, this.title});

  IconData icon;

  /// FOR ASSET IMAGE
  //  String icon;
  String title;
}
