import 'package:flutter/material.dart';
import '../custom/constants.dart';

/**
 * Created by DAEWU on
 * 12, February, 2020
 */

final ThemeData lightTheme = ThemeData(
  backgroundColor: lightBackgroundColor,
  scaffoldBackgroundColor: lightBackgroundColor,
  primaryColor: PrimaryColor,
  accentColor: AccentColor,
  fontFamily: 'Poppins',
  typography: Typography(
    englishLike: Typography.englishLike2018,
    dense: Typography.dense2018,
    tall: Typography.tall2018,
  ),
  textTheme: TextTheme(
    display1: TextStyle(
      fontFamily: 'Poppins',
      fontSize: 34.0,
      color: Colors.black.withOpacity(0.75),
    ),
    headline: TextStyle(
      fontFamily: 'Poppins',
      fontSize: 24.0,
      color: Colors.black.withOpacity(0.65),
    ),
    title: TextStyle(
      fontFamily: 'Poppins',
      fontSize: 20.0,
      color: Colors.black.withOpacity(0.65),
    ),
    subhead: TextStyle(
      fontFamily: 'Poppins',
      fontSize: 16.0,
      color: Colors.black.withOpacity(0.65),
    ),
    body2: TextStyle(
      fontFamily: 'Poppins',
      fontSize: 14.0,
      color: Colors.black.withOpacity(0.65),
    ),
    body1: TextStyle(
      fontFamily: 'Poppins',
      fontSize: 14.0,
      color: Colors.black.withOpacity(0.65),
    ),
  ),
);
