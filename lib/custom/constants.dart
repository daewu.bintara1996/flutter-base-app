import 'dart:ui';

import 'package:flutter/material.dart';
import '../base/base_presenter.dart';

import '../utils/my_widget/UtilitiesWidget.dart';
export '../utils/my_widget/UtilitiesWidget.dart';
export 'package:font_awesome_flutter/font_awesome_flutter.dart';
export '../utils/bottom_navigation/FABBottomAppBar.dart';
export '../utils/bottom_navigation/MyBottomNavBar.dart';

/**
 * Created by DAEWU on
 * 12, February, 2020
 */

// CONFIGS
String APP_NAME = "-- FLUTTER BASE APP --";
var BASE_URL = "https://muslimpeduli.id/";
final String TOKEN_STRING_KEY = 'TOKEN_STRING_KEY';
final String REG_ID = 'REG_ID';
final String IMEI = 'IMEI';
final String DEVICE_NAME = 'DEVICE_NAME';
final String CART_COUNT = 'CART_COUNT';

final String ANDROID_APP_VERSION = '7'; /// always-increment..
final String ANDROID_APP_VERSION_NAME = '1.0.${ANDROID_APP_VERSION}'; /// always-increment..

final String IOS_APP_VERSION = '1'; /// always-increment..
final String IOS_APP_VERSION_NAME = '1.0.${IOS_APP_VERSION}'; /// always-increment..

final String FUCHSIA_APP_VERSION = '1'; /// always-increment..

// Colors
// Colors
const Color lightBackgroundColor = Color(0xFFD6D6D6);
const PrimaryAssentColor = const Color(0xFFC8E6C9);
const BackgroundColor =  Colors.white;
const BackgroundColorAccent =  Color(0xFFF5F5F5);
const PrimaryColor =  Color(0xFF27AE60);
const PrimaryDarkColor =  Color.fromRGBO(224, 103, 12, 1);
const AccentColor =  Color(0xFF27AE60);
const AccentColorDark =  Color.fromRGBO(44, 185, 230, 1);
const ErrorColor =  Colors.deepOrange;
const primarySwatch =  Color.fromRGBO(242, 153, 74, 1);

// Padding
const double paddingZero = 0.0;
const double paddingXS = 2.0;
const double paddingS = 4.0;
const double paddingM = 8.0;
const double paddingL = 16.0;
const double paddingXL = 32.0;

// Margin
const double marginZero = 0.0;
const double marginXS = 2.0;
const double marginS = 4.0;
const double marginM = 8.0;
const double marginL = 16.0;
const double marginXL = 32.0;

// Spacing
const double spaceXS = 2.0;
const double spaceS = 4.0;
const double spaceM = 8.0;
const double spaceL = 16.0;
const double spaceXL = 32.0;

// Assets
/// WRITE YOUR ASSETS BELOW


/// EXPORT MY WIDGET
UtilitiesWidget utilitiesWidget = new UtilitiesWidget();
BasePresenter basePresenter = new BasePresenter();