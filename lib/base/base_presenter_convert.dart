import 'dart:io';

import 'package:baseflutterapp/custom/constants.dart';
import 'package:baseflutterapp/models/BaseRequest.dart';
import 'package:baseflutterapp/router/router.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:meta/meta.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter/foundation.dart' as Foundation;

/**
 * Created by daewubintara on
 * 05, August 2020 10.15
 */

class BasePresenterConvert extends StatelessWidget{

  final String API_NAME = "api/user/";
  BuildContext context;

  Dio dio = Dio();
  BaseRequest baseRequest = BaseRequest();

  Map<String, dynamic> headers(){
    String app_ver = '0';
    if(Platform.isAndroid){
      app_ver = ANDROID_APP_VERSION;
    } else if(Platform.isIOS){
      app_ver = IOS_APP_VERSION;
    }

    Map<String, dynamic> headers = new Map();
    headers['app_version'] = app_ver;
    headers['operating_system'] = Platform.operatingSystem;
    headers[Headers.acceptHeader] = Headers.jsonContentType;
    return headers;
  }

  Future<BaseRequest> getData({
    @required Map<String, dynamic> queryParameters,
    @required String end_point,
    @required bool withToken,
  }) async {
    if (withToken==null) {
      withToken = false;
    }

    final pref = await SharedPreferences.getInstance();
//    String TOKEN = pref.getString(TOKEN_STRING_KEY);
    String TOKEN = "-";

    if(Foundation.kDebugMode){
      print("TOKEN = $TOKEN");
      print("GET URL = ${BASE_URL}${API_NAME}${end_point}");
    }

    var headers = this.headers();
    if(withToken){headers['Authorization'] = "Bearer $TOKEN";}

    String mIMEI = await pref.getString(IMEI);
    headers['imei'] = "$mIMEI";

    try {
      final respons = await dio.get(
        "${BASE_URL}${API_NAME}${end_point}",
        options: Options(headers: headers),
        queryParameters: queryParameters,
      );
      if(Foundation.kDebugMode){
        print("${APP_NAME} ${respons.data.toString()}");
      }
      baseRequest = await BaseRequest.fromJson(respons.data);
      if(baseRequest.rc == "888"){
        _openUpdatePage(baseRequest);
      } else {
        return baseRequest;
      }
    } catch (e) {
      baseRequest.status = false;
      baseRequest.text = "Koneksi server bermasalah, coba beberapa saat lagi";
      return baseRequest;
    }
  }

  Future<BaseRequest> postData({
    @required Map<String, dynamic> data,
    @required String end_point,
    @required bool withToken,
  }) async {
    if (withToken==null) {
      withToken = false;
    }

    final pref = await SharedPreferences.getInstance();
    String TOKEN = pref.getString(TOKEN_STRING_KEY);

    if(Foundation.kDebugMode){
      print("TOKEN = $TOKEN");
      print("POST URL = ${BASE_URL}${API_NAME}${end_point}");
    }

    var headers = this.headers();
    if(withToken){headers['Authorization'] = "Bearer $TOKEN";}

    String mIMEI = await pref.getString(IMEI);
    headers['imei'] = "$mIMEI";

    try {
      final respons = await dio.post(
        "${BASE_URL}${API_NAME}${end_point}",
        options: Options(headers: headers),
        data: data,
      );
      if(Foundation.kDebugMode){
        print("${APP_NAME} ${respons.data.toString()}");
      }
      baseRequest = await BaseRequest.fromJson(respons.data);
      if(baseRequest.rc == "888"){
        _openUpdatePage(baseRequest);
      } else {
        return baseRequest;
      }
    } catch (e) {
      baseRequest.status = false;
      baseRequest.text = "Koneksi server bermasalah, coba beberapa saat lagi";
      return baseRequest;
    }
  }


  _openUpdatePage(BaseRequest data) {
    Navigator.pushReplacementNamed(context, RouterName.main_home);
  }

  // CM DI AMBIL CONTEXTNYA SAJA
  @override
  Widget build(BuildContext context) {
    this.context = context;
    // TODO: implement build
    throw UnimplementedError();
  }

}