import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../custom/constants.dart';
import '../models/BaseRequest.dart';
import 'package:flutter/foundation.dart' as Foundation;

/**
 * Created by DAEWU on
 * 03, March, 2020
 */

class BasePresenter {
  Dio dio = Dio();

  Map<String, dynamic> headers(){
    String app_ver = '0';
    if(Platform.isAndroid){
      app_ver = ANDROID_APP_VERSION;
    } else if(Platform.isIOS){
      app_ver = IOS_APP_VERSION;
    }

    Map<String, dynamic> headers = new Map();
    headers['app_version'] = app_ver;
    headers['operating_system'] = Platform.operatingSystem;
    headers[Headers.acceptHeader] = Headers.jsonContentType;
    return headers;
  }

  Future<BaseRequest> getDataWithToken({
    @required Map<String, dynamic> queryParameters,
    @required String end_point,
  }) async {
    final pref = await SharedPreferences.getInstance();
    String TOKEN = pref.getString(TOKEN_STRING_KEY);

    if(Foundation.kDebugMode){
      print("TOKEN = $TOKEN");
      print("GET URL = ${BASE_URL}api/user/${end_point}");
    }

    var headers = this.headers();
    headers['Authorization'] = "Bearer $TOKEN";

    String mIMEI = await pref.getString(IMEI);
    headers['imei'] = "$mIMEI";

    try {
      final respons = await dio.get(
        "${BASE_URL}api/user/${end_point}",
        options: Options(headers: headers),
        queryParameters: queryParameters,
      );
      if(Foundation.kDebugMode){
        print("${APP_NAME} ${respons.data.toString()}");
      }
      return BaseRequest.fromJson(respons.data);
    } catch (e) {
      return e;
    }
  }



  Future<BaseRequest> getDataFromApi({
    @required Map<String, dynamic> queryParameters,
    @required String end_point,
  }) async {
    var pref = await SharedPreferences.getInstance();

    var headers = this.headers();
    String mIMEI = await pref.getString(IMEI);
    headers['imei'] = "$mIMEI";


    if(Foundation.kDebugMode){
      print("GET URL = ${BASE_URL}api/${end_point}");
    }

    try {
      final respons = await dio.get(
        "${BASE_URL}api/${end_point}",
        options: Options(headers: headers),
        queryParameters: queryParameters,
      );
      if(Foundation.kDebugMode){
        print("${APP_NAME} ${respons.data.toString()}");
      }
      return BaseRequest.fromJson(respons.data);
    } catch (e) {
      return e;
    }
  }


  Future<BaseRequest> getDataNoToken({
    @required Map<String, dynamic> queryParameters,
    @required String end_point,
  }) async {
    var pref = await SharedPreferences.getInstance();

    var headers = this.headers();
    String mIMEI = await pref.getString(IMEI);
    headers['imei'] = "$mIMEI";

    if(Foundation.kDebugMode){
      print("GET URL = ${BASE_URL}api/user/${end_point}");
    }

    try {
      final respons = await dio.get(
        "${BASE_URL}api/user/${end_point}",
        options: Options(headers: headers),
        queryParameters: queryParameters,
      );

      if(Foundation.kDebugMode){
        print("${APP_NAME} ${respons.data.toString()}");
      }
      return BaseRequest.fromJson(respons.data);
    } catch (e) {
      return e;
    }
  }

  Future<BaseRequest> getData({
    @required Map<String, dynamic> queryParameters,
    @required String end_point,
  }) async {
    var pref = await SharedPreferences.getInstance();
    var headers = this.headers();
    String mIMEI = await pref.getString(IMEI);
    headers['imei'] = "$mIMEI";
    if(Foundation.kDebugMode){
      print("GET URL = ${BASE_URL}api/user/${end_point}");
    }

    try {
      final respons = await dio.get(
        "${BASE_URL}api/${end_point}",
        options: Options(headers: headers),
        queryParameters: queryParameters,
      );
      if(Foundation.kDebugMode){
        print("${APP_NAME} ${respons.data.toString()}");
      }
      return BaseRequest.fromJson(respons.data);
    } catch (e) {
      return e;
    }
  }

  Future<BaseRequest> postDataWithToken({
    @required Map<String, dynamic> data,
    @required String end_point,
  }) async {
    final pref = await SharedPreferences.getInstance();
    String TOKEN = pref.getString(TOKEN_STRING_KEY);

    var headers = this.headers();
    headers['Authorization'] = "Bearer $TOKEN";
    String mIMEI = await pref.getString(IMEI);
    headers['imei'] = "$mIMEI";

    if(Foundation.kDebugMode){
      print("TOKEN = $TOKEN");
      print("POST URL = ${BASE_URL}api/user/${end_point}");
    }

    try {
      final respons = await dio.post("${BASE_URL}api/user/${end_point}",
          options: Options(headers: headers), data: data);
      if(Foundation.kDebugMode){
        print("${APP_NAME} ${respons.data.toString()}");
      }
      return BaseRequest.fromJson(respons.data);
    } catch (e) {
      return e;
    }
  }

  Future<BaseRequest> postDataNoToken({
    @required Map<String, dynamic> data,
    @required String end_point,
  }) async {
    var pref = await SharedPreferences.getInstance();

    var headers = this.headers();
    String mIMEI = await pref.getString(IMEI);
    headers['imei'] = "$mIMEI";

    if(Foundation.kDebugMode){
      print("POST URL = ${BASE_URL}api/user/${end_point}");
    }

    try {
      final respons = await dio.post("${BASE_URL}api/user/${end_point}",
          options: Options(headers: headers), data: data);
      if(Foundation.kDebugMode){
        print("${APP_NAME} ${respons.data.toString()}");
      }
      return BaseRequest.fromJson(respons.data);
    } catch (e) {
      return e;
    }
  }

  Future<BaseRequest> postDataMultiPartWithToken({
    @required FormData data,
    @required String end_point,
  }) async {
    final pref = await SharedPreferences.getInstance();
    String TOKEN = pref.getString(TOKEN_STRING_KEY);

    var headers = this.headers();
    headers['Authorization'] = "Bearer $TOKEN";
    String mIMEI = await pref.getString(IMEI);
    headers['imei'] = "$mIMEI";

    if(Foundation.kDebugMode){
      print("TOKEN = $TOKEN");
      print("POST URL = ${BASE_URL}api/user/${end_point}");
    }

    try {
      final respons = await dio.post("${BASE_URL}api/user/${end_point}",
          options: Options(headers: headers), data: data);
      if(Foundation.kDebugMode){
        print("${APP_NAME} ${respons.data.toString()}");
      }
      return BaseRequest.fromJson(respons.data);
    } catch (e) {
      return e;
    }
  }
}
