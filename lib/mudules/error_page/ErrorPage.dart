import 'package:flutter/material.dart';
import '../../custom/constants.dart';

/**
 * Created by daewubintara on
 * 08/04/20 11.59
 */

class ErrorPage extends StatefulWidget {
  String text;
  ErrorPage({this.text});
  @override
  _ErrorPageState createState() => _ErrorPageState(text: text);
}

class _ErrorPageState extends State<ErrorPage> {
  String text;
  _ErrorPageState({this.text});

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    print("DATA : ${text}");
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Container(
            alignment: Alignment.center,
            height: utilitiesWidget.sizeHeight(context),
            width: utilitiesWidget.sizeWidth(context),
            child: CircularProgressIndicator(),
          ),
          Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                image: NetworkImage(text),
                fit: BoxFit.cover,
              ),
            ),
          )
        ],
      ),
    );
  }
}
