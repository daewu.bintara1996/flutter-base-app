import 'package:baseflutterapp/models/BaseRequest.dart';
import 'package:baseflutterapp/mudules/page/bloc/page_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

/**
 * Created by daewubintara on
 * 05, August 2020 11.45
 */
class PageScreen extends StatefulWidget {
  @override
  _PageScreenState createState() => _PageScreenState();
}

class _PageScreenState extends State<PageScreen> {

  _PageScreenState();
  PageBloc pageBloc;

  @override
  void initState() {
    super.initState();
    pageBloc = PageBloc();
    pageBloc.add(LoadPageData());
  }

  @override
  void dispose() {
    pageBloc.close();
    super.dispose();
  }


  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return BlocProvider(
      create: (BuildContext)=>pageBloc,
      child: BlocListener(
        bloc: pageBloc,
        listener: (context, state)=>null,
        child: BlocBuilder(
          bloc: pageBloc,
          builder: (BuildContext context, PageState state){
            if(state is PageInitial){
              return state.widget;
            } else if (state is PageFailedLoadData) {
              return Center(
                child: InkWell(
                  onTap: (){
//                    pageBloc.close();
//                    pageBloc = PageBloc();
                    pageBloc.add(LoadPageData());
                  },
                  child: Text("${state.data.text}"),
                ),
              );
            } else if (state is PageSuccesLoadData){
              return body(state.data);
            } else {
              return Container();
            }
          },
        ),
      ),
    );
  }

  Widget body(BaseRequest data) {
    return Text("${data.text}");
  }

}
