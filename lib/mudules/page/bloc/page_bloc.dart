import 'dart:async';

import 'package:baseflutterapp/base/base_presenter_convert.dart';
import 'package:baseflutterapp/models/BaseRequest.dart';
import 'package:baseflutterapp/utils/my_widget/shimmer/BaseLoadingView.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

part 'page_event.dart';
part 'page_state.dart';

class PageBloc extends Bloc<PageEvent, PageState> {

  BaseRequest data = BaseRequest();
  @override
  // TODO: implement initialState
  PageState get initialState => PageInitial();

  @override
  Stream<PageState> mapEventToState(
    PageEvent event,
  ) async* {
    // TODO: implement mapEventToState
    if (event is LoadPageData){
      data = await BasePresenterConvert().getData(
          queryParameters: {},
          end_point: "v2/mosques",
          withToken: true
      );
      if(data.status){
        yield PageSuccesLoadData(data: data);
      } else {
        yield PageFailedLoadData(data: data);
      }
    }
  }

}
