part of 'page_bloc.dart';

abstract class PageState extends Equatable {
  const PageState();
}

class PageInitial extends PageState {
  Widget widget = BaseLoadingView(type: BaseLoadingView.TYPE_VERTICAL);
  @override
  List<Object> get props => [widget];
}

class PageSuccesLoadData extends PageState {
  BaseRequest data;
  PageSuccesLoadData({this.data});
  @override
  List<Object> get props => [data];
}

class PageFailedLoadData extends PageState {
  BaseRequest data;
  PageFailedLoadData({this.data});
  @override
  List<Object> get props => [data];
}
