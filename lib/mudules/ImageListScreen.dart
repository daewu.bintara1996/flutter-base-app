import 'package:flutter/material.dart';

/**
 * Created by daewubintara on
 * 06, August 2020 10.30
 */
class ImageListScreen extends StatefulWidget {
  @override
  _ImageListScreenState createState() => _ImageListScreenState();
}

class _ImageListScreenState extends State<ImageListScreen> {

  _ImageListScreenState();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }


  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      child: ListView(
        children: List.generate(100, (index) {
          return Padding(
            padding: const EdgeInsets.all(8.0),
            child: ListTile(
              leading: Image.network("http://shift.aplikasiabsensi.com/storage/permits/1595527706.6915-541.webp"),
              title: Text("Gambar $index"),
            ),
          );
        }),
      ),
    );
  }
}
