import 'package:equatable/equatable.dart';

abstract class MainHomeState extends Equatable {
  const MainHomeState();
}

class InitialMainHomeState extends MainHomeState {
  @override
  List<Object> get props => [];
}
