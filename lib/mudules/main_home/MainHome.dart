import 'package:baseflutterapp/custom/constants.dart';
import 'package:baseflutterapp/mudules/ImageListScreen.dart';
import 'package:baseflutterapp/mudules/page/PageScreen.dart';
import 'package:baseflutterapp/router/router.dart';
import 'package:baseflutterapp/utils/my_alert/MyAlertDialog.dart';
import 'package:baseflutterapp/utils/my_widget/LoadingDialog.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import '../../models/childs/Activity.dart';

/**
 * Created by daewubintara on
 * 26, April 2020 09.41
 */
class MainHome extends StatefulWidget {
  Activity kegiatan;

  MainHome({this.kegiatan});

  @override
  _MainHomeState createState() => _MainHomeState(kegiatan: this.kegiatan);
}

class _MainHomeState extends State<MainHome> {
  String title_bar = "Beranda";
  int pageState;
  PageController pageController = PageController();
  Activity kegiatan;
  DateTime currentBackPressTime = DateTime.now();

  _MainHomeState({this.kegiatan});

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    pageState = 0;
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return WillPopScope(
        child: Scaffold(
          appBar: utilitiesWidget.myAppBar(
              context: context, title_bar: title_bar, center_title: false),
          body: PageView(
            children: [
              ImageListScreen(),
              PageScreen(),
              Center(
                child: CupertinoButton.filled(
                  child: Text("${title_bar}"),
                  onPressed: (){
                    Navigator.pushNamed(context, RouterName.main_home_cupertino);
                  },
                ),
              ),
              Center(
                child: Text("${title_bar}"),
              ),
            ],
            controller: pageController,
            physics: NeverScrollableScrollPhysics(),
          ),
          bottomNavigationBar: MyBottomNavBar().MyBottomNavBarRoot(
              currentIndex: pageState,
              backgroundColor: Colors.white,
              elevation: 8.0,
              showUnselectedLabels: true,
              context: context,
              icon_size: 20,
              selectedItemColor: PrimaryColor,
              unselectedItemColor: Colors.grey[500],
              onTap: (index) {
                setState(() {
                  selectedTab(index);
                });
              },
              items: _item_bottom()),
        ),
        onWillPop: onWillPop);
  }

  _item_bottom() {
    return [
      MyBottomNavBarItem(icon: FontAwesomeIcons.home, title: 'Beranda'),
      MyBottomNavBarItem(icon: FontAwesomeIcons.comments, title: 'Pesan'),
      MyBottomNavBarItem(icon: FontAwesomeIcons.history, title: 'Riwayat'),
      MyBottomNavBarItem(icon: FontAwesomeIcons.ellipsisH, title: 'Lainnya'),
    ];
  }

  void selectedTab(int index) {
    setState(() {
      this.pageState = index;
      pageController.jumpToPage(index);
      switch (index) {
        case 0:
          title_bar = "Beranda";
          break;
        case 1:
          title_bar = "Pesan";
          break;
        case 2:
          title_bar = "Riwayat";
          break;
        case 3:
          title_bar = "Lainnya";
          break;
      }
    });
  }

  Future<bool> _onWillPop() async {
    if (pageState != 0) {
      selectedTab(0);
    } else {
      MyAlertDialog.show(context,
          title: "Keluar Aplikasi",
          desc: "Yakin akan keluar dari aplikasi ini?",
          buttons: [
            FlatButton(
              color: Colors.grey,
              child: Text("Cancel",
                  style: Theme.of(context)
                      .textTheme
                      .caption
                      .copyWith(color: Colors.white)),
              onPressed: () {
                MyAlertDialog.hide(context);
              },
            ),
            FlatButton(
              color: Colors.red,
              child: Text("Keluar",
                  style: Theme.of(context)
                      .textTheme
                      .caption
                      .copyWith(color: Colors.white)),
              onPressed: () {
                MyAlertDialog.exitApp(context);
              },
            ),
          ]);
    }
  }

  Future<bool> onWillPop() {
    if (pageState != 0) {
      selectedTab(0);
    } else {
        DateTime now = DateTime.now();
          if (currentBackPressTime == null ||
              now.difference(currentBackPressTime) > Duration(seconds: 2)) {
            currentBackPressTime = now;
            Fluttertoast.showToast(
                msg: "Klik back sekali lagi untuk keluar aplikasi",
                toastLength: Toast.LENGTH_SHORT);
            return Future.value(false);
          }
          return Future.value(true);
        }
    }

}
