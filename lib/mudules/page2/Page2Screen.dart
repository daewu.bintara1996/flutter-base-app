import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

/**
 * Created by daewubintara on
 * 06, August 2020 13.26
 */
class Page2Screen extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return CupertinoTabScaffold(
      tabBar: CupertinoTabBar(
        items: [
          BottomNavigationBarItem(icon: Icon(Icons.message), title: Text("Chat")),
          BottomNavigationBarItem(icon: Icon(Icons.menu), title: Text("Menu")),
        ],
      ),
      tabBuilder: (context, index){
        return CupertinoTabView(
          builder: (context){
            return CupertinoPageScaffold(
              navigationBar: CupertinoNavigationBar(
                middle: Text("Page $index"),
              ),
              child: Center(
                child: CupertinoButton(
                  child: Text("Goto"),
                  onPressed: (){
                    Navigator.of(context).push(
                      CupertinoPageRoute(
                        builder: (context){
                          return CupertinoPageScaffold(
                            navigationBar: CupertinoNavigationBar(
                              middle: Text("Page 2 - tab $index"),
                            ),
                            child: Center(
                              child: CupertinoButton.filled(
                                  child: Text("Back"),
                                  onPressed: (){
                                    Navigator.pop(context);
                                  }
                              ),
                            ),
                          );
                        }
                      )
                    );
                  },
                ),
              ),
            );
          },
        );
      },
    );
  }
}
