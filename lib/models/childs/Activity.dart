/**
 * Created by DAEWU on
 * 05, March, 2020
 */

class Activity {
  int id;
  var mosqueId;
  var title;
  var description;
  var image;
  var image_link;
  var status;
  var tag;
  var longitude;
  var latitude;
  var total_like;
  bool liked;
  bool on_detail;
  var createdAt;
  var updatedAt;
  var activity_date;
  var deletedAt;

  Activity(
      {this.id,
        this.mosqueId,
        this.title,
        this.description,
        this.image,
        this.image_link,
        this.status,
        this.tag,
        this.total_like,
        this.liked,
        this.on_detail = false,
        this.createdAt,
        this.activity_date,
        this.updatedAt,
        this.deletedAt});

  Activity.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    mosqueId = json['mosque_id'];
    title = json['title'];
    description = json['description'];
    image = json['image'];
    image_link = json['image_link'];
    status = json['status'];
    tag = json['tag'];
    longitude = json['longitude'];
    latitude = json['latitude'];
    total_like = json['total_like'];
    liked = json['liked'];
    on_detail = json['on_detail'];
    activity_date = json['activity_date'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    deletedAt = json['deleted_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['mosque_id'] = this.mosqueId;
    data['title'] = this.title;
    data['description'] = this.description;
    data['image'] = this.image;
    data['image_link'] = this.image_link;
    data['status'] = this.status;
    data['tag'] = this.tag;
    data['longitude'] = this.longitude;
    data['latitude'] = this.latitude;
    data['total_like'] = this.total_like;
    data['liked'] = this.liked;
    data['on_detail'] = this.on_detail;
    data['created_at'] = this.createdAt;
    data['activity_date'] = this.activity_date;
    data['updated_at'] = this.updatedAt;
    data['deleted_at'] = this.deletedAt;
    return data;
  }
}