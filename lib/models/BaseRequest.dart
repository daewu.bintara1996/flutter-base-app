import 'childs/Activity.dart';

class BaseRequest {
  bool status;
  var rc;
  String method;
  String link_wa_cs;
  String text;
  String error_text;
  bool open_donasi;
  String token;
  String role;
  String link;
  String about;
  var total_carts;
  var total_mosques;
  var total_trx_delivered;
  var total_donasi_delivered;
  var date_now;
  List<Activity> activities;
  List<Activity> galleries;
  List<Activity> info_registers;

  BaseRequest(
      {this.status,
        this.rc,
        this.about,
        this.method,
        this.text,
        this.error_text,
        this.open_donasi,
        this.link_wa_cs,
        this.token,
        this.role,
        this.link,
        this.total_carts,
        this.total_mosques,
        this.total_trx_delivered,
        this.activities,
        this.galleries,
        this.info_registers,
        this.total_donasi_delivered,
        this.date_now,});

  BaseRequest.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    rc = json['rc'];
    method = json['method'];
    text = json['text'];
    error_text = json['error_text'];
    open_donasi = json['open_donasi'];
    link_wa_cs = json['link_wa_cs'];
    about = json['about'];
    link = json['link'];
    total_mosques = json['total_mosques'];
    total_trx_delivered = json['total_trx_delivered'];
    role = json['role'];
    token = json['token'];
    total_carts = json['total_carts'];
    total_donasi_delivered = json['total_donasi_delivered'];
    date_now = json['date_now'];
    if (json['activities'] != null) {
      activities = new List<Activity>();
      json['activities'].forEach((v) {
        activities.add(new Activity.fromJson(v));
      });
    }
    if (json['galleries'] != null) {
      galleries = new List<Activity>();
      json['galleries'].forEach((v) {
        galleries.add(new Activity.fromJson(v));
      });
    }
    if (json['info_registers'] != null) {
      info_registers = new List<Activity>();
      json['info_registers'].forEach((v) {
        info_registers.add(new Activity.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['rc'] = this.rc;
    data['method'] = this.method;
    data['text'] = this.text;
    data['error_text'] = this.error_text;
    data['open_donasi'] = this.open_donasi;
    data['about'] = this.about;
    data['role'] = this.role;
    data['link_wa_cs'] = this.link_wa_cs;
    data['link'] = this.link;
    data['total_mosques'] = this.total_mosques;
    data['total_trx_delivered'] = this.total_trx_delivered;
    data['total_carts'] = this.total_carts;
    data['token'] = this.token;
    data['total_donasi_delivered'] = this.total_donasi_delivered;
    data['date_now'] = this.date_now;
    if (this.activities != null) {
      data['activities'] = this.activities.map((v) => v.toJson()).toList();
    }
    if (this.galleries != null) {
      data['galleries'] = this.galleries.map((v) => v.toJson()).toList();
    }
    if (this.info_registers != null) {
      data['info_registers'] = this.info_registers.map((v) => v.toJson()).toList();
    }
    return data;
  }
}