import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../custom/constants.dart';

/**
 * Created by DAEWU on
 * 19, February, 2020
 */

class MyFcmManager {
  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
  new FlutterLocalNotificationsPlugin();

  fcmRemote() async {
    final pref = await SharedPreferences.getInstance();
    _firebaseMessaging.getToken().then((String reg_id) {
      print("REG_ID : ${reg_id}");
      pref.setString(REG_ID, reg_id);
    });
    var android = new AndroidInitializationSettings('@mipmap/ic_launcher');
    var iOS = new IOSInitializationSettings();
    var initSetting = new InitializationSettings(android, iOS);
    flutterLocalNotificationsPlugin.initialize(initSetting);

    _firebaseMessaging.configure(onMessage: (Map<String, dynamic> message) {
      print('on message ${message}');
      displayNotification(message);
    });
  }

  Future displayNotification(Map<String, dynamic> message) async {
    final pref = await SharedPreferences.getInstance();
    String TOKEN = pref.getString(TOKEN_STRING_KEY).toString();
    print("TOKEN on fcm = "+TOKEN);

    if (TOKEN != 'null'){
      var androidPlatformChannelSpecifics = new AndroidNotificationDetails(
          'channelid', 'flutterfcm', 'your channel description',
          importance: Importance.Max, priority: Priority.High);
      var iOSPlatformChannelSpecifics = new IOSNotificationDetails();
      var platformChannelSpecifics = new NotificationDetails(
          androidPlatformChannelSpecifics, iOSPlatformChannelSpecifics);
      print('DATA : ${message['data']}');

      await flutterLocalNotificationsPlugin.show(
        0,
        message['notification']['title'],
        message['notification']['body'],
        platformChannelSpecifics,
        payload: '-',
      );
      if(message['data'].toString() != '{}'){
        await flutterLocalNotificationsPlugin.show(
          0,
          message['data']['title'],
          message['data']['message'],
          platformChannelSpecifics,
          payload: '-',
        );
      }
    }

  }

}