import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'custom/themes.dart';
import 'fcm/MyFcmManager.dart';
import 'router/router.dart';

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {

    MyFcmManager().fcmRemote();
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: Colors.transparent,
    ));
    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
    return MaterialApp(
      theme: lightTheme,
      onGenerateRoute: Router.generateRoute,
      initialRoute: RouterName.main_home,
      debugShowCheckedModeBanner: false,
    );
  }
}

void main() => runApp(MyApp());

