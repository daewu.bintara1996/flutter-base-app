import 'package:baseflutterapp/mudules/page2/Page2Screen.dart';
import 'package:flutter/cupertino.dart';

import '../mudules/error_page/ErrorPage.dart';
import '../mudules/main_home/MainHome.dart';
import 'package:flutter/material.dart';
import '../router/router_name.dart';
export '../router/router_name.dart';

/**
 * Created by DAEWU on
 * 12, February, 2020
 */

class Router{
  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch(settings.name){
      case RouterName.main_home:
          return MaterialPageRoute(
            fullscreenDialog: true,
            builder: (_) => MainHome()
          );
        break;
      case RouterName.main_home_cupertino:
        var object = settings.arguments as String;
        return MaterialPageRoute(
            builder: (_) => Page2Screen()
        );
        break;
    }

  }

}