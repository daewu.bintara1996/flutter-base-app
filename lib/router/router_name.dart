/**
 * Created by DAEWU on
 * 12, February, 2020
 */

class RouterName {
  static const String splash = '/splash';
  static const String main_home = '/home';
  static const String main_home_cupertino = '/main_home_cupertino';
  static const String error_page = '/error_page';
  static const String login_register = '/login_register';
}