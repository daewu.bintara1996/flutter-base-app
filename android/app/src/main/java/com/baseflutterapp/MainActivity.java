package com.baseflutterapp;

import android.os.Build;
import android.view.View;

import androidx.annotation.NonNull;
import io.flutter.embedding.android.FlutterActivity;
import io.flutter.embedding.engine.FlutterEngine;
import io.flutter.plugins.GeneratedPluginRegistrant;

import static android.view.View.SYSTEM_UI_FLAG_LIGHT_NAVIGATION_BAR;
import static android.view.WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS;

public class MainActivity extends FlutterActivity {
  @Override
  public void configureFlutterEngine(@NonNull FlutterEngine flutterEngine) {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
      View decorView = getWindow().getDecorView();
      decorView.setSystemUiVisibility(FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS |
              SYSTEM_UI_FLAG_LIGHT_NAVIGATION_BAR);
    }
    GeneratedPluginRegistrant.registerWith(flutterEngine);
  }
}
